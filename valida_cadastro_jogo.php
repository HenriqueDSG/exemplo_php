<?php

    include_once("funcoes.php");
    session_start();

    $cadastro_jogo = $_POST["cadastro_jogo"];
    $conexao = Conectar();

    if ($conexao->connect_error) 
    {
        $_SESSION["mensagem"] = "erro de conexão";
        header("Location: cadastro_jogo.php");
    }

    Query_insert_jogos($conexao, $cadastro_jogo);

    $conexao = Desconectar($conexao);

?>