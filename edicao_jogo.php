<!DOCTYPE html>
<meta charset="utf-8">

<html>

	<head>

		<title> Cadastro de jogos </title>

	</head>

	<body>

		<form method="POST" action="valida_edicao.php">
			
			Nome do jogo: <BR>
			<input type="text" name="edicao_jogo[]">
			<BR>

			Quantidade: <BR>
			<input type="number" name="edicao_jogo[]">
			<BR>

			Valor: R$ <BR>
			<input type="number" name="cadastro_jogo[]" step="0.01">
			<BR><BR>

			<input type="submit" name="salvar" value="Salvar">
			<input type="button" value="Voltar para a Home" onclick="location.href=' home.php'">

		</form>
		
		<?php

            session_start();

			if (!$_SESSION['logado']) 
			{
                header('location: login.php');
			}
			
			if ($_SESSION["logado"] && !$_SESSION["funcionario"]) 
			{
				header("location: home.php");
			}

			if (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'erro de conexão') 
			{
                $_SESSION['mensagem'] = '<BR> <strong> Erro de conexão ao banco de dados! </strong>';
                echo $_SESSION['mensagem'];
				unset($_SESSION['mensagem']);				
			} 

			elseif (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'informações incompletas') 
			{
                $_SESSION['mensagem'] = '<BR> <strong> Informações de cadastro incompletas! </strong>';
                echo $_SESSION['mensagem'];
                unset($_SESSION['mensagem']);
			} 
			
			elseif (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'sucesso') {
                $_SESSION['mensagem'] = '<BR> <strong> Jogo cadastrado com sucesso! </strong>';
                echo $_SESSION['mensagem'];
                unset($_SESSION['mensagem']);
            }
        ?>

	</body>

</html>