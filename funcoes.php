<?php

    session_start();
    
    function Conectar()
	{
		$servidor = "localhost";
		$user     = "root";
		$senha    = "";
		$banco    = "loja_jogos";

		$conexao = new mysqli($servidor, $user, $senha, $banco);

		return $conexao;
    }

    function Desconectar($conexao)
    {
        $conexao->close();
    }
    
    function Query_cadastro_funcionario($conexao, $cadastro, $funcionario)
    {
        $query_cadastro_funcionario = $conexao->prepare("INSERT INTO loja_jogos.usuario (nome, sexo,  data_nascimento, email, endereco, funcionario, senha) VALUES (?, ?, ?, ?, ?, ?, ?);");
        $query_cadastro_funcionario->bind_param('sssssss', $cadastro[0], $cadastro[1], $cadastro[2], $cadastro[3], $cadastro[4], $funcionario, $cadastro[5]);
        $query_cadastro_funcionario->execute();

        if ($cadastro[0] == null || $cadastro[3] == null) 
        {
            $_SESSION["mensagem"] = "nome ou email não preenchido";
        }

        elseif ($cadastro[5] != $cadastro[6]) 
        {
            $_SESSION["mensagem"] = "erro na senha";
        }

        else
        {
            $_SESSION["mensagem"] = "sucesso";
        }

        header("Location: cadastro.php");  
    }

    function Query_cadastro($conexao, $cadastro)
    {
        $query_cadastro = $conexao->prepare("INSERT INTO loja_jogos.usuario (nome, sexo, data_nascimento, email, endereco, senha) VALUES (?, ?, ?, ?, ?, ?);");
        $query_cadastro->bind_param('ssissi', $cadastro[0], $cadastro[1], $cadastro[2], $cadastro[3], $cadastro[4], $cadastro[5]);
        $query_cadastro->execute();

        if ($cadastro[0] == null || $cadastro[3] == null) 
        {
            $_SESSION["mensagem"] = "nome ou email não preenchido";
        }

        elseif ($cadastro[5] != $cadastro[6]) 
        {
            $_SESSION["mensagem"] = "erro na senha";
        }

        else
        {
            $_SESSION["mensagem"] = "sucesso";
        }

        header("Location: cadastro.php")
    }

    function Query_login($conexao, $login)
    {     
        $query_select_login = $conexao->prepare("SELECT email, senha FROM loja_jogos.usuario WHERE email = '?' and senha = '?';");
		$query_select_login->bind_param('si', $login[0], $login[1]);
        $query_select_login->execute();
        $query_select_login->num_rows;

        if ($query_select_login >= 1) 
        {
            $_SESSION["mensagem"] = "sucesso";
            $_SESSION["usuario"] = $login[0];
            $_SESSION["logado"] = true;
        }

        else 
        {
            $_SESSION["mensagem"] = "Você deve se cadastrar para poder acessar o sistema";      
            header("Location: login.php");    
        }
    }

    function Éfuncionario($conexao, $login)
    {     
        $query_select_login = $conexao->prepare("SELECT funcionario FROM loja_jogos.usuarios WHERE nome_usuario = '?' and funcionario = 'S';");
		$query_select_login->bind_param('s', $login);
        $query_select_login->execute();
        $query_select_login->num_rows;

        if ($query_select_login >= 1) 
        {
            $_SESSION["funcionario"] = true;
        }

        header("Location: home.php");       
    }

    function Query_update_jogos($conexao, $cadastro_jogo)
    {
        if ($cadastro_jogo[0] <> null || $cadastro_jogo[1] <> null || $cadastro_jogo[2] <> null) 
        {
            for ($i = 0; $i < 3;) {
                if (isset($cadastro_jogo[$i])) 
                {
                    if ($i == 0) {$campo = "nome_jogo";}
                    if ($i == 1) {$campo = "classificacao_etaria";}
                    if ($i == 2) {$campo = "valor_unitario";}

                    $query_update_jogo = $conexao->prepare("UPDATE loja_jogos.jogos SET (?) VALUES (?);");
                    $query_update_jogo->bind_param('si', $campo , $cadastro_jogo[$i]);
                    $query_update_jogo->execute();
                    
                }
                $i++;
            }

           $_SESSION["mensagem"] = "sucesso";
       }

       else 
       {           
           $_SESSION["mensagem"] = "Não há modificações!";         
       }

        header("Location: cadastro_jogo.php");
    }

    function ConsultaNome($conexao, $busca_jogo)
    {
            //$nome_jogo = "%'".$nome_jogo."'%";
            $query_busca_jogos_todos = $conexao->prepare("SELECT * FROM loja_jogos.estoque_jogos WHERE nome_jogo like ?;");
            $query_busca_jogos_todos->bind_param('s', $busca_jogo);
            $query_busca_jogos_todos->execute(); 
            //$query_busca_jogos_todos->close();
            $_SESSION["teste"] = $query_busca_jogos_todos;

            return $query_busca_jogos_todos;
    }

    function Query_insert_jogos($conexao, $cadastro_jogo)
    {
        $query_insert_jogo = $conexao->prepare("INSERT INTO loja_jogos.estoque_jogos (nome_jogo, quant_jogo, valor_jogo) VALUES (?, ?, ?);");
        $query_insert_jogo->bind_param('sii', $cadastro_jogo[0], $cadastro_jogo[1], $cadastro_jogo[2]);
        $query_insert_jogo->execute();
        
        if ($cadastro_jogo[0] == null || $cadastro_jogo[1] == null || $cadastro_jogo[2] == null) 
        {
            $_SESSION["mensagem"] = "informações incompletas";
        }

        else 
        {           
            $_SESSION["mensagem"] = "sucesso";         
        }

        header("Location: cadastro_jogo.php");
    }

    function Query_busca_jogos_todos($conexao)
    {
        $query_busca_jogos_todos = $conexao->prepare("SELECT * FROM loja_jogos.jogos;");
        $query_busca_jogos_todos->execute();

        while ($aux = $query_busca_jogos_todos->fetch_array()) 
        {          
            if ($query_busca_jogos_todos != null) 
            {
                  //$query_busca_jogos_todos->store_result();
                  //$query_busca_jogos_todos->bind_result($nome_jogo, $classificacao_etaria, $valor_unitario);
            
                  echo '<tr>';
                        echo '<td>' . $aux["nome"] . '</td>';
                        echo '<td>' . $aux["classificacao"] . '</td>';
                        echo '<td>' . $aux["valor"] . '</td>';
                  echo '</tr>';
            }  
         }                             

        return $query_busca_jogos_todos;
    }

    function log_out()
    {
        session_start();
        unset($_SESSION["logado"]);
        session_destroy();
        header("location: login.php");
    }

    function ListarJogos($conexao, $busca_jogo)
    {
        $query_busca = $conexao->prepare("SELECT * FROM loja_jogos.estoque_jogos WHERE nome_jogo LIKE '%?%';");
        $query_busca->bind_param('s', $busca_jogo);
        $query_busca->execute();
        $con = query($query_busca) || die($query_busca->error);
        return $con;
    }
    
?>