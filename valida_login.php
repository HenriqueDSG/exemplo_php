<?php

    include_once('funcoes.php');
    session_start();

    $login = $_POST['login'];

    $conexao = Conectar();

    if ($conexao->connect_error) 
    {
        $_SESSION['mensagem'] = 'erro de conexão';
        header('Location: login.php');
    } 
    
    else 
    {
        Query_login($conexao, $login);

        if ($_SESSION["logado"] == true) 
        {
            Éfuncionario($conexao, $login);
        }       
    }
    $conexao = Desconectar($conexao);

?>