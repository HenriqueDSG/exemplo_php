create database loja_jogos;

use loja_jogos;

create table usuario
(
	id_usuario int primary key,
    email varchar(45) unique,
	nome varchar(45) not null, 
    data_nascimento date not null,
    sexo Enum('F','M') not null,
    endereco varchar(45) not null,
    senha varchar(45) not null,
    funcionario Enum('S','N') not null
);

create table estoque_jogos
(
	id_jogo int auto_increment primary key,
	nome_jogo varchar (45) not null,
    quant_jogo int not null,
    valor_jogo decimal(6,2) not null
);

create table funcionarios
(
	id_funcionario int auto_increment primary key,
    id_usuario int unique not null,
    
   CONSTRAINT `fk_id_usuario` FOREIGN KEY ( `id_usuario` ) REFERENCES `usuario` ( `id_usuario` )
);

create table pedido
(
	id_pedido int auto_increment primary key,
    id_usuario int not null,
    total decimal(6, 2) not null,
    data_hora datetime not null,
    
	CONSTRAINT `fk_id_usuarios` FOREIGN KEY ( `id_usuario` ) REFERENCES `usuario` ( `id_usuario` )
);

create table pedido_jogo
(
	idpedido int not null primary key,
    id_jogo int not null,
    id_pedido int not null,
    
    CONSTRAINT `fk_idpedido` FOREIGN KEY ( `idpedido` ) REFERENCES `pedido` ( `id_pedido` ),
    CONSTRAINT `fk_id_jogo` FOREIGN KEY ( `id_jogo` ) REFERENCES `estoque_jogos` ( `id_jogo` )
);