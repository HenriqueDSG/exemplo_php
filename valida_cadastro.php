<?php 
       		
    include_once("funcoes.php");    
    session_start();
    
    $cadastro = $_POST["cadastro"];
    $funcionario = $_POST["funcionario"];
    $conexao = Conectar();

    if ($conexao->connect_error) 
    {
        $_SESSION["mensagem"] = "erro de conexão";
        header("Location: login.php");
    }

    if (isset($funcionario)) 
    {
        Query_cadastro_funcionario($conexao, $cadastro, $funcionario);
    }

    else 
    {
        Query_cadastro($conexao,$cadastro);
    }

    $conexao = Desconectar($conexao);
                    
?>