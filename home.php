<!DOCTYPE html>
<meta charset="utf-8">

<html>

	<head>

		<title> Home </title>

	</head>

	<body>

	<?php

        session_start();

        if (!$_SESSION['logado']) 
        {
            header('location: login.php');
        }

        if (isset($_SESSION['login']))
         {
            header('Location: login.php');
        }

        if (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'sucesso') 
        {
            $_SESSION['mensagem'] = '<BR> <strong> Bem vindo '.$_SESSION['usuario'].'! </strong> <BR>';
            echo '<BR>'. $_SESSION['mensagem'] .'<BR>';
            unset($_SESSION['mensagem']);
        }

        if (isset($_SESSION["mensagem"]) && $_SESSION["mensagem"] == "sem acesso")
        {
            $_SESSION["mensagem"] = "<BR> <strong> Acesso não permitido! Somente a funcionarios! </strong> <BR>";
            echo '<BR>'. $_SESSION['mensagem'] .'<BR>';
            unset($_SESSION['mensagem']);
        }

    ?>

		<form method="POST">
			
            <input type="button" name="logout" value="Logout" onclick="location.href='logout.php'">
            <BR><BR><BR>
            
            <input type="button" name="cadastrar" value="Cadastrar Jogos" onclick="location.href='cadastro_jogo.php'">
			<input type="button" name="busca" value="Busca de Jogos" onclick="location.href='busca_jogo.php'">
			<input type="button" name="edita" value="Editar Jogo" onclick="location.href='edicao_jogo.php'">
			<input type="button" name="compra" value="Compra Jogos" onclick="location.href='compra_jogos.php'">

		</form>
			
	</body>

</html>