<!DOCTYPE html>
<meta charset="utf-8">

<html>

	<head>

		<title> Cadastro de Usuário </title>

	</head>

	<body>

		<form method="POST" action="valida_cadastro.php">
				
			Nome completo:
			<input type="text" name="cadastro[]" required> &nbsp &nbsp

			Sexo: 
			<select name="cadastro[]">
				<option value="M"> Masculino </option>
				<option value="F"> Feminino </option>
			</select>
			&nbsp &nbsp

			Data de nascimento:
			<input type="date" name="cadastro[]" required> &nbsp &nbsp

			Email de Login de usuário:
			<input type="email" name="cadastro[]" required> &nbsp &nbsp<BR>

			Endereço:
			<input type="text" name="cadastro[]" required> &nbsp &nbsp

			Funcionário:
			<input type="checkbox" name="funcionario"> &nbsp &nbsp

			Senha de usuário:
			<input type="password" name="cadastro[]" required> &nbsp &nbsp

			Confirmação de senha:
			<input type="password" name="cadastro[]" required> <BR>

			<input type="submit" name="submit" value="Salvar">
			<input type="button" name="login" value="Voltar a Pagina Anterior" onclick="location.href='login.php'">

		</form>

		<?php

            session_start();

			if (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'erro de conexão') 
			{
                $_SESSION['mensagem'] = '<BR> <strong> Erro de conexão ao banco de dados! </strong>';
                echo $_SESSION['mensagem'];
                unset($_SESSION['mensagem']);
			} 
			
			elseif (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'nome ou email não preenchido') 
			{
                $_SESSION['mensagem'] = '<BR> <strong> Erro de cadastro! Nome ou E-mail não preenchidos! </strong>';
                echo $_SESSION['mensagem'];
                unset($_SESSION['mensagem']);
			} 
			
			elseif (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'erro na senha') 
			{
                $_SESSION['mensagem'] = '<BR> <strong> Erro de cadastro! A senha e a confirmação de senha não conferem! </strong>';
                echo $_SESSION['mensagem'];
                unset($_SESSION['mensagem']);
			} 
			
			elseif (isset($_SESSION['mensagem']) && $_SESSION['mensagem'] == 'sucesso') 
			{
                $_SESSION['mensagem'] = '<BR> <strong> Cadastro realizado com sucesso! </strong>';
                echo $_SESSION['mensagem'];
                unset($_SESSION['mensagem']);
            }

        ?>	

	</body>

</html>